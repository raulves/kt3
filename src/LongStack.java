import java.util.LinkedList;

public class LongStack {

   public static void main (String[] argum) {
      String s = "2 5 SWAP -";

      System.out.println(LongStack.interpret(s));
   }

   private LinkedList<Long> list;

   LongStack() {
      list = new LinkedList<>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack clone = new LongStack();
      if(this.list.size() > 0){

         for (int i = this.list.toArray().length - 1; i >= 0; i--){
            clone.push((Long) this.list.toArray()[i]);
         }
      }
      return clone;
   }

   public boolean stEmpty() {
      return list.isEmpty();
   }

   public void swap() {
      if(list.size() < 2) {
         throw new RuntimeException("Not enough elements in Linked list to swap.");
      }
      long firstElement = pop();
      long secondElement = pop();
      push(firstElement);
      push(secondElement);
   }

   public void rot() {
      if(list.size() < 3) {
         throw new RuntimeException("Not enough elements in Linked list to rotate.");
      }
      long firstElement = pop();
      long secondElement = pop();
      long thirdElement = pop();
      push(secondElement);
      push(firstElement);
      push(thirdElement);
   }

   public void push (long a) {
      list.addFirst(a);
   }

   public long pop() {
      if(stEmpty()){
         throw new RuntimeException("Linked list is empty");
      }
      return list.removeFirst();
   }

   public void op (String s) {
      if(list.size() < 2) {
         throw new RuntimeException("Not enough elements in Linked list.");
      }

      if(s.equals("+") || s.equals("-") || s.equals("/") || s.equals("*")) {
         long first = pop();
         long second = pop();

         switch (s) {
            case "+":
               push(second + first);
               break;
            case "-":
               push(second - first);
               break;
            case "*":
               push(second * first);
               break;
            case "/":
               push(second / first);
               break;
         }
      } else {
         throw new RuntimeException("Invalid operation " + s);
      }
   }
  
   public long tos() {
      if (stEmpty()){
         throw new RuntimeException("Linked list is empty");
      }
      return list.getFirst();
   }

   @Override
   public boolean equals (Object o) {
      if (((LongStack) o).list.size() != this.list.size()) {
         return false;
      }

      if (stEmpty()) {
         if (((LongStack) o).stEmpty()){
            return true;
         }
         return false;
      }
      if (((LongStack) o).stEmpty()) {
         if (stEmpty()){
            return true;
         }
         return false;
      }

      if (((LongStack) o).tos() != this.tos()) {
         return false;
      }

      for (int i = 0; i < this.list.size(); i++) {
         if (!((LongStack) o).list.get(i).equals(this.list.get(i)))
            return false;
      }
      return true;
   }

   @Override
   public String toString() {
      StringBuffer result = new StringBuffer();
      for(int i = list.toArray().length - 1; i >= 0; i--){
         result.append(list.toArray()[i]);
      }
      return result.toString();
   }

   public static long interpret (String pol) {
      if (pol.isEmpty()){
         throw new RuntimeException("Empty expression");
      }
      LongStack stack = new LongStack();
      String[] elements = pol.split(" ");
      for(String elem : elements){
         if(elem.trim().length() == 0) {
            continue;
         }
         if(elem.equals("+") || elem.equals("-") || elem.equals("/") || elem.equals("*")) {
            try {
               stack.op(elem);
            }
            catch (RuntimeException e) {
               throw new RuntimeException("Cannot perform " + elem + " in expression " + '"' + pol + '"');
            }

         } else if (elem.trim().toLowerCase().equals("swap")) {
            try {
               stack.swap();
            }
            catch (RuntimeException e) {
               throw new RuntimeException("Cannot perform " + elem + " in expression " + '"' + pol + '"');
            }
         } else if (elem.trim().toLowerCase().equals("rot")) {
            try {
               stack.rot();
            }
            catch (RuntimeException e) {
               throw new RuntimeException("Cannot perform " + elem + " in expression " + '"' + pol + '"');
            }
         }
         else if (elem.trim().matches("[-+]?\\d*\\.?\\d+")) {
            stack.push(Long.parseLong(elem.trim()));

         } else {
            throw new RuntimeException("Illegal symbol " + elem + " in expression " + '"' + pol + '"');
         }
      }
      if(stack.list.size() > 1){
         throw new RuntimeException("Too many numbers in expression " + '"' + pol + '"');
      }
      return stack.tos();
   }

}

